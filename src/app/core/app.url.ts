import { Component, Inject } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';

@Component({
  selector: 'app-root',
  template: '<div>just for show</div>'
})
export class AppComponent {
  constructor(@Inject(APP_BASE_HREF) baseHref: string) { }
}