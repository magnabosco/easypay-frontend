import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http'
import { Observable } from 'rxjs';
import { tap} from 'rxjs/operators';
import { Router } from '@angular/router';


@Injectable()
export class InterceptService implements HttpInterceptor {

    constructor(private router: Router){}
    
    intercept(request: HttpRequest<any>, next: HttpHandler):Observable<HttpEvent<any>> {

        let auth = JSON.parse(window.sessionStorage.getItem('token'));
        if (!auth) {
            this.router.navigate(['login']);   
        }

        if(!request.url.endsWith("/token")){
            request = request.clone({
                setHeaders: {
                  Authorization: 'Bearer ' + auth.access_token
                }
            });
        }

	    return next.handle(request).pipe(
	        tap(event => {}, error => {
                if(error.status === 401){
                    this.router.navigate(['login']);
                }
	        })
	    )
    };
 
}