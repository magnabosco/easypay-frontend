import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { User } from "../model/user.model";
import { Globals } from "../Globals";

@Injectable()
export class ApiService {

  constructor(private http: HttpClient) { }
  baseUrl: string = Globals.apiUrl + '/users/';

  login(loginPayload) {
    const headers = {
      'Authorization': 'Basic ' + btoa('easypay:easypay'),
      'Content-type': 'application/x-www-form-urlencoded'
    }
    return this.http.post('http://192.168.0.111:8081/' + 'dev/oauth/token', loginPayload, {headers});
  }

  getUsers() {
    return this.http.get('http://192.168.0.111:8081/dev/boleto?codigo=1');
  }

  getUserById(id: number) {
    return this.http.get(this.baseUrl + 'user/' + id + '?access_token=' + JSON.parse(window.sessionStorage.getItem('token')).access_token);
  }

  createUser(user: User){
    return this.http.post(this.baseUrl + 'user?access_token=' + JSON.parse(window.sessionStorage.getItem('token')).access_token, user);
  }

  updateUser(user: User): Observable<Object> {
    return this.http.put(this.baseUrl + 'user/' + user.id + '?access_token=' + JSON.parse(window.sessionStorage.getItem('token')).access_token, user);
  }

  deleteUser(id: number){
    return this.http.delete(this.baseUrl + 'user/' + id + '?access_token=' + JSON.parse(window.sessionStorage.getItem('token')).access_token);
  }
}
