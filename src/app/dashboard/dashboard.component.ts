import { Component, OnInit } from '@angular/core';
import { ApiService } from '../core/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private router: Router, private apiService: ApiService) { }

  ngOnInit() {

    this.apiService.getUsers().subscribe(data => {
      this.router.navigate(['dashboard']);
    });

  }

}
